/* global TimelineMax, Power4, EB, EBG */

// Broadcast Events shim
// ====================================================================================================
(function() {
    if (typeof window.CustomEvent === 'function') { return false; }

    function CustomEvent(event, params) {
        params = params || { bubbles: false, cancelable: false, detail: undefined };
        var evt = document.createEvent('CustomEvent');
        evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
        return evt;
    }

    CustomEvent.prototype = window.Event.prototype;
    window.CustomEvent = CustomEvent;
})();

// Timeline
// ====================================================================================================
var timeline = (function MasterTimeline() {

    var tl;
    var win = window;

    function doClickTag() { window.open(window.clickTag); }

    function initTimeline() {
        document.querySelector('#ad .banner').style.display = 'block';
        document.getElementById('ad').addEventListener('click', doClickTag);
        createTimeline();
    }

    function createTimeline() {
        tl = new TimelineMax({delay: 0.25, onStart: updateStart, onComplete: updateComplete, onUpdate: updateStats});
        // ---------------------------------------------------------------------------

        tl.add('frame1')
        .from('.nocs', 1.25, { opacity:0, ease: Power3.easeInOut }, 'frame1+=0.5')
        .from('.hl1', 1.25, { x:"100%", ease: Power3.easeInOut }, 'frame1+=0.5')
        .to('.nocs', 1, { y:"-100%", ease: Power3.easeInOut }, 'frame1+=2.25')

        // ---------------------------------------------------------------------------
        tl.add('frame2')
        
        .from('.hl2', 1.25, { opacity:0, ease: Power3.easeInOut }, 'frame2+=.25')
        .from('.scope', 1.25, { y:"-100%", ease: Power3.easeInOut }, 'frame2')
        .to('.hl2', 1.25, { opacity:0, ease: Power3.easeInOut }, 'frame2+=2')
        .to('.hl1', 1.25, { opacity:0, ease: Power3.easeInOut }, 'frame2+=2')
        .to('.scope', 1.25, { opacity:0, ease: Power3.easeInOut }, 'frame2+=2')

        // ---------------------------------------------------------------------------

        tl.add('frame3')
        .from('.bg', 1, { opacity:0, ease: Power3.easeInOut }, 'frame3')
        .from('.logo', 1, { x:"100%", ease: Power3.easeInOut }, 'frame3+=.5')
        .from('.hl3', 1, { x:"100%", ease: Power3.easeInOut }, 'frame3+=1.5')
        .from('.cta', 1, { x:"100%", ease: Power3.easeInOut }, 'frame3+=2.75')
    




        // ---------------------------------------------------------------------------

        // DEBUG:
        // tl.play('frame3'); // start playing at label:frame3
        // tl.pause('frame3'); // pause the timeline at label:frame3
    }

    function updateStart() {
        var start = new CustomEvent('start', {
            'detail': { 'hasStarted': true }
        });
        win.dispatchEvent(start);
    }

    function updateComplete() {
        var complete = new CustomEvent('complete', {
            'detail': { 'hasStopped': true }
        });
        win.dispatchEvent(complete);
    }

    function updateStats() {
        var statistics = new CustomEvent('stats', {
            'detail': { 'totalTime': tl.totalTime(), 'totalProgress': tl.totalProgress(), 'totalDuration': tl.totalDuration()
            }
        });
        win.dispatchEvent(statistics);
    }

    function getTimeline() {
        return tl;
    }

    return {
        init: initTimeline,
        get: getTimeline
    };

})();

// Banner Init
// ====================================================================================================
timeline.init();
